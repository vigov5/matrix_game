defmodule MatrixGameWeb.Router do
  use MatrixGameWeb.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", MatrixGameWeb do
    # Use the default browser stack
    pipe_through :browser

    get "/", PageController, :index
    get "/game/:game_id", PageController, :game
  end

  scope "/api", MatrixGameWeb do
    pipe_through :api

    get "/games", APIController, :games
  end

  # Other scopes may use custom stacks.
  # scope "/api", MatrixGame do
  #   pipe_through :api
  # end
end
