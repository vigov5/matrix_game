defmodule MatrixGameWeb.PageController do
  use MatrixGameWeb.Web, :controller
  alias MatrixGame.Game.Supervisor, as: GameSupervisor

  def index(conn, _params) do
    id =
      case get_session(conn, :player_id) do
        nil ->
          MatrixGame.generate_player_id()
        player_id ->
          player_id
      end

    modes_text = %{"easy" => "7⇢10", "normal" => "6⇢10", "hard" => "5⇢10"}

    render(put_session(conn, :player_id, id), "index.html",
      id: id,
      games: GameSupervisor.current_games(),
      modes_text: modes_text
    )
  end

  def game(conn, %{"game_id" => game_id}) do
    id =
      case get_session(conn, :player_id) do
        nil ->
          MatrixGame.generate_player_id()
        player_id ->
          player_id
      end

    game =
      case MatrixGame.Game.get_data(game_id) do
        {:error, "Game does not exist"} ->
          %{locked: false, max_player: 0, players: []}

        game ->
          game
      end

    render(put_session(conn, :player_id, id), "game.html", id: id, game_id: game_id, game: game)
  end
end
