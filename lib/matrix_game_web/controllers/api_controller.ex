defmodule MatrixGameWeb.APIController do
  use MatrixGameWeb.Web, :controller
  alias MatrixGame.Game.Supervisor, as: GameSupervisor

  def games(conn, _params) do
    games = GameSupervisor.current_games()
    render(conn, "games.json", games: games)
  end
end
