defmodule MatrixGameWeb.APIView do
  use MatrixGameWeb.Web, :view
  alias MatrixGameWeb.GameView

  def render("games.json", %{games: games}) do
    %{data: render_many(games, GameView, "game.json")}
  end
end
