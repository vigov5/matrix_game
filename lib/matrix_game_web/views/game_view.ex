defmodule MatrixGameWeb.GameView do
  use MatrixGameWeb.Web, :view

  def render("game.json", %{game: game}) do
    %{
      id: game.id,
      players: game.players,
      points: game.points,
      phase: game.phase,
      max_player: game.max_player,
      turn: game.turn,
      player_states: game.player_states,
      player_nicks: game.player_nicks,
      mode: game.mode,
      spectators: game.spectators,
      spectator_nicks: game.spectator_nicks,
      time_limit: game.time_limit,
      locked: game.locked
    }
  end
end
