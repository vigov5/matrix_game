defmodule MatrixGameWeb.GameChannel do
  @moduledoc """
  Game channel
  """
  use Phoenix.Channel
  alias MatrixGame.Game
  alias MatrixGame.Game.Board
  alias MatrixGame.Game.Supervisor, as: GameSupervisor
  require Logger

  def join("game:" <> game_id, %{"nick" => nick, "password" => password}, socket) do
    Logger.debug("Joining Game channel #{game_id}", game_id: game_id)

    player_id = socket.assigns.player_id

    case Game.join(game_id, player_id, nick, password, socket.channel_pid) do
      {:ok, pid} ->
        Process.monitor(pid)

        data = Game.get_data(game_id, player_id)

        {:ok, %{game: data}, assign(socket, :game_id, game_id)}

      {:error, reason} ->
        Logger.debug("Error joining with reason #{reason}")
        {:error, %{reason: reason}}

      {:spectator, _} ->
        data = Game.get_data(game_id, player_id)

        socket =
          socket
          |> assign(:game_id, game_id)
          |> assign(:spectator_id, player_id)

        {:ok, %{game: data}, socket}
    end
  end

  def handle_in("game:get_data", _message, socket) do
    player_id = socket.assigns.player_id
    game_id = socket.assigns.game_id

    data = Game.get_data(game_id, player_id)

    {:reply, {:ok, %{game: data}}, socket}
  end

  def handle_in("game:send_message", %{"text" => text}, socket) do
    Logger.debug("Handling send_message on GameChannel #{socket.assigns.game_id}")

    player_id = socket.assigns.player_id
    message = %{player_id: player_id, text: text}

    broadcast!(socket, "game:message_sent", %{message: message})

    {:noreply, socket}
  end

  def handle_in("game:place_piece", %{"y" => y, "x" => x, "turn" => current_turn}, socket) do
    Logger.debug("Handling place_piece on GameChannel #{socket.assigns.game_id}")

    player_id = socket.assigns.player_id
    game_id = socket.assigns.game_id

    Logger.debug("Player #{player_id} placed piece at coordinates #{y} #{x}")

    case Game.place_piece(game_id, player_id, x: x, y: y, turn: current_turn) do
      {:ok, %Game{phase: :over} = game} ->
        broadcast(socket, "game:over", %{game: game})
        {:noreply, socket}

      {:ok, game} ->
        {:reply, {:ok, %{game: game}}, socket}

      {:error, reason} ->
        {:reply, {:error, %{reason: reason}}, socket}

      _ ->
        {:reply, {:error, %{reason: "Something went wrong while placing"}}, socket}
    end
  end

  def handle_in("game:set_ready", %{"ready" => true}, socket) do
    Logger.debug("Handling get_ready on GameChannel #{socket.assigns.game_id}")
    player_id = socket.assigns.player_id
    game_id = socket.assigns.game_id

    case Game.set_ready(game_id, player_id) do
      {:ok, game} ->
        {:reply, {:ok, %{game: game}}, socket}

      {:error, reason} ->
        {:reply, {:error, %{reason: reason}}, socket}
    end
  end

  def terminate(reason, %{assigns: %{game_id: game_id, spectator_id: spectator_id}} = socket) do
    # TODO broadcast spectator left
    Game.spectator_left(game_id, spectator_id)

    Logger.debug(
      "Terminating GameChannel #{socket.assigns.game_id} #{inspect(reason)} for spectator #{
        spectator_id
      }"
    )

    :ok
  end

  def terminate(reason, %{assigns: %{game_id: game_id, player_id: player_id}} = socket) do
    Logger.debug("Terminating GameChannel #{game_id} #{inspect(reason)}")

    # check in case board already terminated
    # TODO remove nick requirement
    case GenServer.whereis(Board.ref(game_id, player_id)) do
      nil ->
        :ok

      _ ->
        nick = Board.get_data(game_id, player_id).nick

        broadcast(socket, "game:player_left", %{player_id: nick})

        case Game.player_left(game_id, player_id) do
          {:ok, %{phase: :over} = game} ->
            Logger.debug("Stop Game !")
            GameSupervisor.stop_game(game_id)

            broadcast(socket, "game:over", %{game: game})
            :ok

          {:ok, %{phase: :finished}} ->
            :ok

          _ ->
            :ok
        end
    end
  end

  def terminate(reason, %{assigns: %{player_id: player_id}}) do
    Logger.debug("Terminating GameChannel for player #{player_id} #{inspect(reason)}")
    :ok
  end

  def handle_info(_, socket), do: {:noreply, socket}

  def broadcast_stop(game_id) do
    Logger.debug("Broadcasting game:stopped from GameChannel #{game_id}")

    MatrixGameWeb.Endpoint.broadcast("game:#{game_id}", "game:stopped", %{})
  end

  def broadcast_new_piece(game_id, values) do
    Logger.debug("Broadcasting game:new_piece from GameChannel #{game_id} with value #{values}")

    MatrixGameWeb.Endpoint.broadcast("game:#{game_id}", "game:new_piece", %{piece: values})
  end

  def broadcast_place_piece(game_id, player_id, piece, point) do
    Logger.debug(
      "Broadcasting game:place_piece from GameChannel #{game_id} by player #{player_id}"
    )

    MatrixGameWeb.Endpoint.broadcast("game:#{game_id}", "game:place_piece", %{
      player_id: player_id,
      piece: piece,
      game: point
    })
  end

  def broadcast_player_joined(game_id, _, game) do
    Logger.debug("Broadcasting game:player_joined from GameChannel #{game_id}")

    MatrixGameWeb.Endpoint.broadcast("game:#{game_id}", "game:player_joined", %{game: game})
  end
end
