defmodule MatrixGameWeb.LobbyChannel do
  @moduledoc """
  Lobby channel
  """
  require Logger

  use MatrixGameWeb.Web, :channel
  alias MatrixGame.Game.Supervisor, as: GameSupervisor

  def join("lobby", _msg, socket) do
    {:ok, socket}
  end

  def handle_in("current_games", _params, socket) do
    {:reply, {:ok, %{games: GameSupervisor.current_games()}}, socket}
  end

  def handle_in(
        "new_game",
        %{
          "max_player" => max_player,
          "mode" => mode,
          "time_limit" => time_limit,
          "password" => password
        },
        socket
      )
      when max_player in ["2", "3", "4"] and
             mode in ["easy", "medium", "hard"] and
             time_limit in ["0", "15", "30"] do
    game_id = MatrixGame.generate_game_id()

    GameSupervisor.create_game(
      game_id,
      String.to_integer(max_player),
      mode,
      String.to_integer(time_limit),
      password
    )

    {:reply, {:ok, %{game_id: game_id}}, socket}
  end

  def handle_in("new_game", _params, socket) do
    {:reply, {:error, %{reason: "Invalid game settings !"}}, socket}
  end

  def broadcast_current_games do
    Logger.debug("Broadcasting current games from LobbyChannel")

    MatrixGameWeb.Endpoint.broadcast("lobby", "update_games", %{
      games: GameSupervisor.current_games()
    })
  end
end
