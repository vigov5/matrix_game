defmodule MatrixGame.Game.EventHandler do
  use GenServer
  require Logger
  alias MatrixGameWeb.{LobbyChannel, GameChannel}

  def init(init_arg) do
    {:ok, init_arg}
  end

  def handle_cast({:new_piece, game_id, values}, state) do
    broadcast_update(state)
    GameChannel.broadcast_new_piece(game_id, values)

    {:noreply, state}
  end

  def handle_cast({:player_joined, game_id, player_id, game}, state) do
    broadcast_update(state)
    GameChannel.broadcast_player_joined(game_id, player_id, game)

    {:noreply, state}
  end

  def handle_cast({:place_piece, game_id, player_id, piece, game}, state) do
    broadcast_update(state)
    GameChannel.broadcast_place_piece(game_id, player_id, piece, game)

    {:noreply, state}
  end

  def handle_cast({:game_stopped, game_id}, state) do
    broadcast_update(state)
    GameChannel.broadcast_stop(game_id)

    {:noreply, state}
  end

  def handle_cast({:game_over, game_id}, state) do
    broadcast_update(state)
    GameChannel.broadcast_stop(game_id)

    {:noreply, state}
  end

  def handle_cast({:spectator_joined, _, _, _}, state), do: broadcast_update(state)
  def handle_cast({:spectator_left, _, _, _}, state), do: broadcast_update(state)
  def handle_cast({:game_started, _}, state), do: broadcast_update(state)
  def handle_cast(_, state), do: {:noreply, state}

  defp broadcast_update(state) do
    LobbyChannel.broadcast_current_games()

    {:noreply, state}
  end
end
