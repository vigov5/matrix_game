defmodule MatrixGame.Game.EventManager do
  require Logger
  @timeout 30_000

  def start_link() do
    import Supervisor.Spec

    child = worker(GenServer, [], restart: :transient)
    {:ok, manager} = Supervisor.start_link([child], name: __MODULE__, strategy: :simple_one_for_one)

    handlers = [
      MatrixGame.Game.EventHandler
    ]
    Enum.each(handlers, &__MODULE__.add_handler(__MODULE__, &1, []))

    {:ok, manager}
  end

  def stop(sup) do
    for {_, pid, _, _} <- Supervisor.which_children(sup) do
      GenServer.stop(pid, :normal, @timeout)
    end

    Supervisor.stop(sup)
  end

  def add_handler(sup, handler, opts) do
    Supervisor.start_child(sup, [handler, opts])
  end

  def game_created(sup) do
    notify(sup, {:game_created})
  end

  def player_joined(sup, game_id, player_id, game) do
    notify(sup, {:player_joined, game_id, player_id, game})
  end

  def spectator_joined(sup, game_id, player_id, game) do
    notify(sup, {:spectator_joined, game_id, player_id, game})
  end

  def spectator_left(sup, game_id, player_id, game) do
    notify(sup, {:spectator_left, game_id, player_id, game})
  end

  def game_started(sup, game_id) do
    notify(sup, {:game_started, game_id})
  end

  def game_over(sup, game_id) do
    notify(sup, {:game_over, game_id})
  end

  def game_stopped(sup, game_id) do
    notify(sup, {:game_stopped, game_id})
  end

  def place_piece(sup, game_id, player_id, piece, game) do
    notify(sup, {:place_piece, game_id, player_id, piece, game})
  end

  def new_piece(sup, game_id, values) do
    notify(sup, {:new_piece, game_id, values})
  end

  defp notify(sup, msg) do
    for {_, pid, _, _} <- Supervisor.which_children(sup) do
      GenServer.cast(pid, msg)
    end

    :ok
  end
end
