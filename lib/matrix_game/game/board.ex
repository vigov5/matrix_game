defmodule MatrixGame.Game.Board do
  require Logger
  alias MatrixGame.Game.Piece

  @size 9
  @cell_size 3
  @row_size div(@size, @cell_size)

  @grid_empty_value -1

  defimpl Jason.Encoder, for: MatrixGame.Game.Board do
    def encode(value, opts) do
      Jason.Encode.map(value, opts)
    end
  end

  defstruct player_id: nil,
            nick: nil,
            grid: %{},
            points: 0,
            game_id: nil

  @doc """
  Creates a new board for a Player
  """
  def create(game_id, player_id, nick) do
    Logger.debug("Starting board for player #{player_id}", ansi_color: :green)

    grid = build_grid()

    Agent.start(
      fn ->
        %__MODULE__{player_id: player_id, nick: nick, grid: grid, points: 0, game_id: game_id}
      end,
      name: ref(game_id, player_id)
    )
  end

  @doc """
  Destroys an existing Player board
  """
  def destroy(game_id, player_id) do
    case GenServer.whereis(ref(game_id, player_id)) do
      nil ->
        Logger.debug("Attempt to destroy unesxisting Board for player #{player_id}",
          ansi_color: :green
        )

      pid ->
        Logger.debug("Stopping board for player #{player_id}", ansi_color: :green)
        Agent.stop(pid, :normal, :infinity)
    end
  end

  @doc """
  Place piece
  """
  def place_piece(_game_id, _player_id, %Piece{x: x}) when x > @size - 1 or x < 0,
    do: {:error, "Invalid col position"}

  def place_piece(_game_id, _player_id, %Piece{y: y}) when rem(y, @row_size) != 0 or y < 0,
    do: {:error, "Invalid row position"}

  def place_piece(game_id, player_id, piece) do
    Logger.debug("Place piece #{inspect(piece)} for player #{player_id}", ansi_color: :green)

    board = Agent.get(ref(game_id, player_id), & &1)

    cond do
      piece_with_invalid_coordinates?(board, piece) ->
        {:error, "Piece has invalid coordinates"}

      true ->
        new_board =
          board
          |> place_piece_to_grid(piece)
          |> update_points

        Agent.update(ref(game_id, player_id), fn _ -> new_board end)

        {:ok, new_board}
    end
  end

  @doc """
  Returns the board
  """
  def get_data(game_id, player_id) do
    Logger.debug("Getting board state for player #{player_id}", ansi_color: :green)

    board = Agent.get(ref(game_id, player_id), & &1)
    %{board | grid: serialize_grid(board)}
  end

  # @doc """
  # Returns the raw board
  # """
  def get_raw_data(game_id, player_id) do
    Agent.get(ref(game_id, player_id), & &1)
  end

  # Generates global reference name for the board process
  def ref(game_id, player_id), do: {:global, {:game, game_id, :board, player_id}}

  defp piece_with_invalid_coordinates?(board, piece) do
    piece
    |> Piece.coordinates()
    |> Enum.map(&(board.grid[&1] != @grid_empty_value))
    |> Enum.any?(& &1)
  end

  def list_valid_coordinates(game_id, player_id) do
    board = Agent.get(ref(game_id, player_id), & &1)

    rows = 0..(@row_size - 1) |> Enum.map(&(&1 * @cell_size))

    for(col <- 0..(@size - 1), row <- rows, do: {row, col})
    |> Enum.filter(&(board.grid[&1] == @grid_empty_value))
  end

  # Place piece to the grid
  defp place_piece_to_grid(board, piece) do
    coordinates =
      piece
      |> Piece.coordinates()
      |> Enum.zip(piece.values)
      |> Enum.into(%{})

    grid = Map.merge(board.grid, coordinates)

    %{board | grid: grid}
  end

  # Builds a default grid map
  def build_grid do
    0..(@size - 1)
    |> Enum.reduce([], &build_rows/2)
    |> List.flatten()
    |> Enum.reduce(%{}, fn item, acc -> Map.put(acc, item, @grid_empty_value) end)
  end

  # Builds cells for a given row
  def build_rows(y, rows) do
    row =
      0..(@size - 1)
      |> Enum.reduce(rows, fn x, col -> [{y, x} | col] end)

    [row | rows]
  end

  def update_points(board) do
    values =
      Enum.map(board.grid, fn {{row, col}, _} ->
        x =
          Enum.map(
            [
              row_list(row, col),
              col_list(row, col),
              left_cross_list(row, col),
              right_cross_list(row, col)
            ],
            fn x ->
              Enum.any?(x, &same_values?(board, &1))
            end
          )

        x |> Enum.filter(& &1) |> Enum.count() |> Kernel.*(board.grid[{row, col}])
      end)

    %{board | points: Enum.sum(values)}
  end

  def same_values?(board, triple) do
    value = board.grid[List.first(triple)]
    Enum.all?(triple, fn x -> board.grid[x] == value and board.grid[x] != @grid_empty_value end)
  end

  def row_list(row, col) do
    (col - 1)..(col + 1)
    |> Enum.reduce([], fn x, acc -> acc ++ [[{row, x - 1}, {row, x}, {row, x + 1}]] end)
    |> Enum.filter(&valid_triple?(&1))
  end

  def col_list(row, col) do
    (row - 1)..(row + 1)
    |> Enum.reduce([], fn x, acc -> acc ++ [[{x - 1, col}, {x, col}, {x + 1, col}]] end)
    |> Enum.filter(&valid_triple?(&1))
  end

  def right_cross_list(row, col) do
    -1..1
    |> Enum.reduce([], fn x, acc ->
      acc ++ [[{row + x - 1, col - x + 1}, {row + x, col - x}, {row + x + 1, col - x - 1}]]
    end)
    |> Enum.filter(&valid_triple?(&1))
  end

  def left_cross_list(row, col) do
    -1..1
    |> Enum.reduce([], fn x, acc ->
      acc ++ [[{row + x - 1, col + x - 1}, {row + x, col + x}, {row + x + 1, col + x + 1}]]
    end)
    |> Enum.filter(&valid_triple?(&1))
  end

  def valid_triple?(triple) do
    Enum.all?(triple, &valid_coordinates?(&1))
  end

  def valid_coordinates?({row, col}) do
    row > -1 and row < @size and col > -1 and col < @size
  end

  def serialize_grid(board) do
    Enum.reduce(board.grid, %{}, fn {{row, col}, v}, acc ->
      Map.merge(acc, %{Enum.join([row, col], "") => v})
    end)
  end
end
