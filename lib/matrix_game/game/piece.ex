defmodule MatrixGame.Game.Piece do
  @moduledoc """
  One Piece
  """

  @mode_easy_range 7..10
  @mode_medium_range 6..10
  @mode_hard_range 5..10

  defimpl Jason.Encoder, for: MatrixGame.Game.Piece do
    def encode(value, opts) do
      Jason.Encode.map(value, opts)
    end
  end

  defstruct x: 0,
            y: 0,
            values: []

  def coordinates(%{x: x, y: y, values: _values}) do
    Enum.map(y..(y + 2), &coordinate_key(&1, x))
  end

  defp coordinate_key(y, x), do: {y, x}

  def generate(mode) do
    %{
      x: 0,
      y: 0,
      values: gen_values(mode)
    }
  end

  defp gen_values(mode) do
    range =
      case mode do
        "easy" ->
          @mode_easy_range

        "medium" ->
          @mode_medium_range

        "hard" ->
          @mode_hard_range

        _ ->
          @mode_easy_range
      end

    [Enum.random(range), Enum.random(range), Enum.random(range)]
  end
end
