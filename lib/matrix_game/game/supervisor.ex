defmodule MatrixGame.Game.Supervisor do
  @moduledoc """
  Game processes supervisor
  """
  require Logger

  use Supervisor
  alias MatrixGame.{Game}
  alias MatrixGame.Game.EventManager

  def start_link, do: Supervisor.start_link(__MODULE__, :ok, name: __MODULE__)

  def init(:ok) do
    children = [
      worker(Game, [], restart: :temporary)
    ]

    supervise(children, strategy: :simple_one_for_one)
  end

  @doc """
  Creates a new supervised Game process
  """
  def create_game(id, max_player, mode, time_limit, password),
    do: Supervisor.start_child(__MODULE__, [id, max_player, mode, time_limit, password])

  @doc """
  Returns a list of the current games
  """
  def current_games do
    __MODULE__
    |> Supervisor.which_children()
    |> Enum.map(&game_data/1)
  end

  def stop_game(game_id) do
    Logger.debug("Stopping game #{game_id} in supervisor")

    case GenServer.whereis({:global, {:game, game_id}}) do
      nil ->
        Logger.debug("Game #{game_id} belong to another node")

      pid ->
        Supervisor.terminate_child(__MODULE__, pid)
    end
  end

  defp game_data({_id, pid, _type, _modules}) do
    pid
    |> GenServer.call(:get_data)
    |> Map.take([
      :id,
      :players,
      :points,
      :phase,
      :max_player,
      :turn,
      :player_states,
      :player_nicks,
      :mode,
      :spectators,
      :spectator_nicks,
      :time_limit,
      :locked,
      :created_at
    ])
  end

  def handle_info({:EXIT, _pid, reason}, _state) do
    Logger.info("A child process died: #{reason}")
  end

  def handle_info(msg, state) do
    Logger.error("Supervisor received unexpected message: #{inspect(msg)}")
    {:noreply, state}
  end

  def sweep_game do
    Logger.info("Start sweeping abandoned games")
    thresh_hold = Application.get_env(:matrix_game, :max_abandoned_time)

    __MODULE__
    |> Supervisor.which_children()
    |> Enum.map(&game_data/1)
    |> Enum.filter(fn game ->
      DateTime.diff(DateTime.utc_now(), game.created_at) > thresh_hold and
        length(game.players) == 0
    end)
    |> Enum.map(fn game ->
      EventManager.game_over(EventManager, game.id)
      stop_game(game.id)
    end)
  end
end
