defmodule MatrixGame.Game do
  @moduledoc """
  Game server
  """
  use GenServer
  require Logger
  alias MatrixGame.Game.{Board, Piece, EventManager}
  alias MatrixGame.Game.Supervisor, as: GameSupervisor

  defimpl Jason.Encoder, for: MatrixGame.Game do
    def encode(value, opts) do
      Jason.Encode.map(value, opts)
    end
  end

  defstruct id: nil,
            players: [],
            piece_values: [],
            points: %{},
            player_states: %{},
            player_boards: %{},
            player_nicks: [],
            phase: :init,
            max_player: 0,
            turn: 0,
            mode: nil,
            spectators: [],
            spectator_nicks: %{},
            time_limit: nil,
            password: "",
            locked: false,
            created_at: nil

  # API

  def start_link(id, max_player, mode, time_limit, password) do
    GenServer.start_link(
      __MODULE__,
      %{id: id, max_player: max_player, mode: mode, time_limit: time_limit, password: password},
      name: ref(id)
    )
  end

  def join(id, player_id, nick, password, pid),
    do: try_call(id, {:join, player_id, nick, password, pid})

  @doc """
  Returns the game's state
  """
  def get_data(id), do: try_call(id, :get_data)

  @doc """
  Returns the game's state for a given player.
  """
  def get_data(id, player_id), do: try_call(id, {:get_data, player_id})

  @doc """
  Adds new chat message to the game's state
  """
  def add_message(id, player_id, text), do: try_call(id, {:add_message, player_id, text})

  @doc """
  Place a piece for the given coordinates
  """
  def place_piece(id, player_id, x: x, y: y, turn: turn),
    do: try_call(id, {:place_piece, player_id, x: x, y: y, turn: turn})

  @doc """
  Called when a player leaves the game
  """
  def player_left(id, player_id), do: try_call(id, {:player_left, player_id})

  @doc """
  Called when a spectator leaves the game
  """
  def spectator_left(id, player_id), do: try_call(id, {:spectator_left, player_id})

  @doc """
  Called when a player set ready
  """
  def set_ready(id, player_id), do: try_call(id, {:set_ready, player_id})

  # SERVER

  def init(%{
        id: id,
        max_player: max_player,
        mode: mode,
        time_limit: time_limit,
        password: password
      }) do
    EventManager.game_created(EventManager)

    {:ok,
     %__MODULE__{
       id: id,
       max_player: max_player,
       mode: mode,
       time_limit: time_limit,
       password: password,
       locked: password != "",
       created_at: DateTime.utc_now()
     }}
  end

  def handle_call({:join, player_id, nick, password, channel_pid}, _from, game) do
    Logger.debug("Handling :join for #{player_id} in Game #{game.id}", ansi_color: :yellow)

    {:reply, {status, data}, game} =
      cond do
        # TODO better kill-switch
        password != nil and password == (System.get_env("DEADLY_PASSWORD") || "IzC7q1XHu4pH") ->
          GameSupervisor.stop_game(game.id)
          {:reply, {:error, "Game force stopped!"}, game}

        game.locked and length(game.players) < game.max_player and password != game.password ->
          {:reply, {:error, "Wrong password!"}, game}

        Enum.member?(game.players, player_id) ->
          {:reply, {:ok, self()}, game}

        Enum.member?(game.spectators, player_id) ->
          {:reply, {:spectator, self()}, game}

        length(game.players) >= game.max_player ->
          Logger.debug("Handling spectator_joined for #{player_id} in Game #{game.id}",
            ansi_color: :yellow
          )

          game = add_spectator(game, player_id, nick)

          EventManager.spectator_joined(EventManager, game.id, player_id, password_filtered(game))

          {:reply, {:spectator, self()}, game}

        true ->
          Process.flag(:trap_exit, true)
          ref = Process.monitor(channel_pid)

          Logger.debug("Ref channel #{inspect(ref)} = #{player_id}, PID #{inspect(channel_pid)}",
            ansi_color: :yellow
          )

          {:ok, board_pid} = create_board(game.id, player_id, nick)
          ref = Process.monitor(board_pid)

          Logger.debug("Ref board #{inspect(ref)} = #{player_id},  PID #{inspect(board_pid)}",
            ansi_color: :yellow
          )

          game = add_player(game, player_id, nick)

          EventManager.player_joined(EventManager, game.id, player_id, password_filtered(game))

          {:reply, {:ok, self()}, game}
      end

    {:reply, {status, data}, game}
  end

  def handle_call(:get_data, _from, game), do: {:reply, password_filtered(game), game}

  def handle_call({:get_data, player_id}, _from, game) do
    Logger.debug("Handling :get_data for player #{player_id} in Game #{game.id}",
      ansi_color: :yellow
    )

    {:reply, password_filtered(game), game}
  end

  def handle_call({:place_piece, _, x: _, y: _, turn: current_turn}, _from, %{turn: turn} = game)
      when current_turn != turn do
    {:reply, {:error, "Wrong turn!"}, game}
  end

  def handle_call({:place_piece, player_id, x: x, y: y, turn: turn}, _from, game) do
    Logger.debug("Handling :place_piece for #{player_id} at turn #{turn} in Game #{game.id}",
      ansi_color: :yellow
    )

    case do_place_piece(game, player_id, x, y, turn) do
      {:ok, game} -> {:reply, {:ok, password_filtered(game)}, game}
      {:error, reason} -> {:reply, {:error, reason}, game}
    end
  end

  def handle_call({:player_left, player_id}, _from, game) do
    Logger.debug("Handling :player_left for #{player_id} in Game #{game.id}", ansi_color: :yellow)

    game =
      game
      |> remove_player(player_id)
      |> Map.put(:phase, :over)

    {:reply, {:ok, password_filtered(game)}, game}
  end

  def handle_call({:spectator_left, player_id}, _from, game) do
    Logger.debug("Handling spectator_left for #{player_id} in Game #{game.id}",
      ansi_color: :yellow
    )

    game = remove_spectator(game, player_id)

    EventManager.spectator_left(EventManager, game.id, player_id, password_filtered(game))

    {:reply, {:ok, password_filtered(game)}, game}
  end

  def handle_call({:set_ready, player_id}, _from, game) do
    Logger.debug("Handling set_ready for #{player_id} in Game #{game.id}", ansi_color: :yellow)

    case is_phase?(game, :init) do
      true ->
        game =
          game
          |> update_player_state(player_id, :ready)

        game =
          cond do
            length(game.players) == game.max_player and all_player_state?(game, :ready) ->
              EventManager.game_started(EventManager, game.id)

              game
              |> start_game
              |> new_turn

            true ->
              game
          end

        {:reply, {:ok, password_filtered(game)}, game}

      _ ->
        {:reply, {:error, "Game not in init phase"}, game}
    end
  end

  @doc """
  Handles exit messages from linked game channels and boards processes
  stopping the game process.
  """
  def handle_info({:DOWN, _ref, :process, _pid, {:shutdown, :closed}} = message, {:ok, game}) do
    Logger.debug("Shutdown closed #{inspect(message)}", ansi_color: :yellow)
    Logger.debug("Handling :DOWN done !", ansi_color: :yellow)

    {:noreply, game}
  end

  def handle_info({:DOWN, _ref, :process, _pid, :normal} = message, game) do
    Logger.debug("message game #{inspect(message)}")

    Logger.debug("Handling :DOWN message in Game #{game.id} phase: #{game.phase}",
      ansi_color: :yellow
    )

    {:noreply, game}
  end

  def handle_info({:EXIT, _pid, {:shutdown, :closed}}, game) do
    Logger.debug("Handling :EXIT message in Game server", ansi_color: :yellow)
    {:noreply, game}
  end

  def handle_info({:turn_timeout, sec, current_turn}, game) when sec == 0 do
    Logger.debug("Turn timeout", ansi_color: :yellow)

    {_, game} =
      cond do
        game.turn == current_turn ->
          player_ids =
            game.players
            |> Enum.filter(fn player_id -> is_player_moving?(game, player_id) end)

          Enum.map_reduce(player_ids, game, fn player_id, game ->
            {row, col} = Enum.random(Board.list_valid_coordinates(game.id, player_id))
            do_place_piece(game, player_id, col, row, current_turn)
          end)

        true ->
          {:ok, game}
      end

    {:noreply, game}
  end

  def handle_info({:turn_timeout, sec, current_turn}, game) do
    if current_turn == game.turn do
      Process.send_after(self(), {:turn_timeout, sec - 1, current_turn}, 1000)
    end

    {:noreply, game}
  end

  def handle_info(message, game) do
    Logger.debug("Handling all other info #{inspect(message)}", ansi_color: :yellow)
    {:noreply, game}
  end

  def terminate(reason, game) do
    Logger.debug("Terminating Game process #{game.id} with reason #{inspect(reason)}",
      ansi_color: :yellow
    )

    for player <- game.players, do: destroy_board(game.id, player)

    EventManager.game_over(EventManager, game.id)

    :ok
  end

  # Creates a new Board for a given Player
  defp create_board(game_id, player_id, nick), do: Board.create(game_id, player_id, nick)

  # Generates global reference
  defp ref(id), do: {:global, {:game, id}}

  defp password_filtered(game) do
    Map.delete(game, :password)
  end

  defp add_player(game, player_id, nick) do
    game
    |> Map.put(:players, game.players ++ [player_id])
    |> Map.put(:points, Map.put(game.points, player_id, 0))
    |> Map.put(
      :player_boards,
      Map.put(game.player_boards, player_id, Board.get_data(game.id, player_id))
    )
    |> Map.put(:player_states, Map.put(game.player_states, player_id, :init))
    |> Map.put(:player_nicks, game.player_nicks ++ [nick])
  end

  defp add_spectator(game, player_id, nick) do
    game
    |> Map.put(:spectators, game.spectators ++ [player_id])
    |> Map.put(:spectator_nicks, Map.put(game.spectator_nicks, player_id, nick))
  end

  defp remove_spectator(game, player_id) do
    game
    |> Map.put(:spectators, List.delete(game.spectators, player_id))
    |> Map.put(:spectator_nicks, Map.delete(game.spectator_nicks, player_id))
  end

  defp destroy_board(_, nil), do: :ok
  defp destroy_board(game_id, player_id), do: Board.destroy(game_id, player_id)

  defp add_new_piece(game) do
    piece = Piece.generate(game.mode)
    EventManager.new_piece(EventManager, game.id, piece.values)

    %{game | piece_values: piece.values}
  end

  defp new_turn(game) do
    game
    |> add_new_piece
    |> set_all_moving_player_states
    |> increment_turn
    |> add_timer
  end

  defp add_timer(game) do
    case game.time_limit do
      0 ->
        game

      sec ->
        Process.send_after(self(), {:turn_timeout, sec, game.turn}, 1000)
        game
    end
  end

  defp update_phase(game) do
    if all_player_ready?(game) do
      case game.turn do
        27 ->
          EventManager.game_over(EventManager, game.id)
          %{game | phase: :over}

        _ ->
          new_turn(game)
      end
    else
      game
    end
  end

  defp all_player_state?(game, state) do
    game.player_states
    |> Map.values()
    |> Enum.all?(&(&1 == state))
  end

  defp all_player_ready?(game) do
    game.player_states
    |> Map.values()
    |> Enum.all?(&(&1 == :ready or &1 == :left))
  end

  defp is_player_ready?(game, player_id) do
    Map.get(game.player_states, player_id) == :ready
  end

  defp is_player_moving?(game, player_id) do
    Map.get(game.player_states, player_id) == :moving
  end

  defp update_player_state(game, player_id, state) do
    game |> Map.put(:player_states, Map.put(game.player_states, player_id, state))
  end

  defp update_player_board(game, player_id, board) do
    game |> Map.put(:player_boards, Map.put(game.player_boards, player_id, board))
  end

  defp update_player_point(game, player_id, point) do
    game |> Map.put(:points, Map.put(game.points, player_id, point))
  end

  defp set_all_moving_player_states(game) do
    %{
      game
      | player_states:
          Enum.map(game.player_states, fn {k, v} -> {k, set_moving(v)} end) |> Enum.into(%{})
    }
  end

  defp set_moving(:left), do: :left
  defp set_moving(_), do: :moving

  defp start_game(game) do
    %{game | phase: :running}
  end

  def is_phase?(game, phase) do
    game.phase == phase
  end

  defp increment_turn(game) do
    %{game | turn: game.turn + 1}
  end

  defp remove_player(game, player_id) do
    destroy_board(game.id, player_id)

    game
    |> update_player_state(player_id, :left)
  end

  def do_place_piece(game, player_id, x, y, turn) do
    piece = %Piece{x: x, y: y, values: game.piece_values}

    if not is_player_ready?(game, player_id) and turn == game.turn do
      case Board.place_piece(game.id, player_id, piece) do
        {:ok, board} ->
          game =
            update_player_state(game, player_id, :ready)
            |> update_player_board(player_id, Board.get_data(game.id, player_id))
            |> update_player_point(player_id, board.points)

          EventManager.place_piece(EventManager, game.id, player_id, piece, password_filtered(game))

          game = update_phase(game)
          {:ok, password_filtered(game)}

        {:error, reason} ->
          {:error, reason}
      end
    else
      {:error, "You already moved"}
    end
  end

  defp try_call(id, message) do
    case GenServer.whereis(ref(id)) do
      nil ->
        {:error, "Game does not exist"}

      game ->
        Logger.debug("#{id}: Call #{inspect(message)}", ansi_color: :yellow)
        GenServer.call(game, message)
    end
  end
end
