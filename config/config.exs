# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :matrix_game,
  ecto_repos: [MatrixGameWeb.Repo]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Configures the endpoint
config :matrix_game, MatrixGameWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "Slz4NpuzBuzd+xNbchBfH6VzJn6OHz7ick4Z0ZCKiWE+0YbNxWhFFMfYSz10uGbz",
  render_errors: [view: MatrixGameWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: MatrixGame.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

config :matrix_game,
  ga_tracking_code: "",
  # Number of bytes for generic unique id
  id_length: 8,
  # Number of words used in game ids "ahoy-matey"
  id_words: 2,
  # Maximal number >= 100 after the words "ahoey-matey-9999".
  id_number_max: 9999,
  # Maximal time before empty room is destroyed
  max_abandoned_time: 60

config :matrix_game, MatrixGame.Scheduler,
  jobs: [
    # Every minute
    {"@minutely", {MatrixGame.Game.Supervisor, :sweep_game, []}}
  ]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
