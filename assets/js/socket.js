// NOTE: The contents of this file will only be executed if
// you uncomment its entry in "web/static/js/app.js".

// To use Phoenix channels, the first step is to import Socket
// and connect at the socket path in "lib/my_app/endpoint.ex":
import {Socket} from "phoenix"

let updateGame = (game) => {
  let elm = $("#" + game.id);
  let isNew = !elm.length;
  elm = $(".dummy_game").clone();

  for (let index = 0; index < 4; index++) {
    let i = index + 1;
    if (game.players.length >= i || game.players.length == 0) {
      elm.children().find(`#p${i}-nick`).text(game.player_nicks[index]);
      elm.children().find(`#p${i}-point`).text(game.points[game.players[index]]);
    } else {
      elm.find(`#p${i}-info`).remove();
    }
  }

  let modes = {
    "easy": "7⇢10",
    "medium": "6⇢10",
    "hard": "5⇢10"
  }

  elm.find('#game_id').text(`(${game.id})`);
  elm.find('#game_info').text(`[${game.players.length}/${game.max_player}]`);
  elm.find('#game_mode').text(`{${modes[game.mode]}}`);
  elm.find('#game_spec').text(`${game.spectators.length}`);
  elm.find('#game_time_limit').text(`${game.time_limit}`);
  if (game.players.length == game.max_player) {
    elm.find('#game_link').text("WATCH");
  }
  elm.find('#game_locked').text(game.locked ? 'lock' : 'lock_open');
  elm.find('#game_link').attr("href", "/game/" + game.id);

  elm.find('#game_turn').text(`(${game.turn})`);
  if (isNew) {
    $("#list_games").append('<li class="collection-item" id="' + game.id + '">' + elm.html() + "</li>");
  } else {
    $("#" + game.id).html(elm.html());
  }

  $("#" + game.id).find("#join_game").on("click", resp => {
    if (!checkNick()) {
      resp.preventDefault();
    } else {
      window.location = $(resp.target).attr('href') || $(resp.target).find("#game_link").attr('href');
    }
  });
}

let matrix_alert = (content) => {
  $("#alert_content").html(content);
  $("#matrix_dialog").show();
}

$("#alert_close_btn").click(function(){
  $("#matrix_dialog").hide();
});

let cleanUpGames = (games) => {
  let gameIds = games.map(x => x.id);
  $("#list_games").children().filter((k, v) => { return gameIds.indexOf(v.id) == -1 }).remove();
}

let socket = new Socket("/socket", {params: {id: window.PlayerId}})
socket.connect()
// When you connect, you'll often need to authenticate the client.
// For example, imagine you have an authentication plug, `MyAuth`,
// which authenticates the session and assigns a `:current_user`.
// If the current user exists you can assign the user's token in
// the connection for use in the layout.
//
// In your "web/router.ex":
//
//     pipeline :browser do
//       ...
//       plug MyAuth
//       plug :put_user_token
//     end
//
//     defp put_user_token(conn, _) do
//       if current_user = conn.assigns[:current_user] do
//         token = Phoenix.Token.sign(conn, "user socket", current_user.id)
//         assign(conn, :user_token, token)
//       else
//         conn
//       end
//     end
//
// Now you need to pass this token to JavaScript. You can do so
// inside a script tag in "web/templates/layout/app.html.eex":
//
//     <script>window.userToken = "<%= assigns[:user_token] %>";</script>
//
// You will need to verify the user token in the "connect/2" function
// in "web/channels/user_socket.ex":
//
//     def connect(%{"token" => token}, socket) do
//       # max_age: 1209600 is equivalent to two weeks in seconds
//       case Phoenix.Token.verify(socket, "user socket", token, max_age: 1209600) do
//         {:ok, user_id} ->
//           {:ok, assign(socket, :user, user_id)}
//         {:error, reason} ->
//           :error
//       end
//     end
//
// Finally, pass the token on connect as below. Or remove it
// from connect if you don't care about authentication.

let checkNick = () => {
  let nick = $("#nick").val();
  if (!nick) {
    matrix_alert("Please enter your nick name !");
    return false;
  } else if (nick.length > 10) {
    matrix_alert("Nick name is 10 chars or less !");
    return false;
  } else {
    localStorage.setItem('nick', nick);
    return true;
  }
}

if (window.location.pathname == "/") {
  $("#nick").val(localStorage.getItem('nick'));
  // Now that you are connected, you can join channels with a topic:
  let channel = socket.channel("lobby", {})
  channel.join()
    .receive("ok", resp => { console.log("Joined successfully", resp) })
    .receive("error", resp => { console.log("Unable to join", resp) })

  $("#join_game").on("click", resp => {
    if (!checkNick()) {
      resp.preventDefault();
    } else {
      window.location = $(resp.target).find("#game_link").attr('href');
    }
  })

  $("#set_nick").on("click", resp => {
    if (checkNick()) matrix_alert('Nickname set! Hello, ' + $("#nick").val() + '!');
  });

  $("#create_game").on("click", resp => {
    if (checkNick()) {
      channel.push("new_game", {
        max_player: $('input[name=max_player]:checked').val(),
        mode: $('input[name=mode]:checked').val(),
        time_limit: $('input[name=time_limit]:checked').val(),
        password: $('#password').val(),
      })
      .receive("ok", resp => {
        console.log("Created successfully", resp);
        window.location = "/game/" + resp.game_id;
      })
      .receive("error", resp => { matrix_alert(resp.reason) });
    }
  })

  channel.on('current_games', (payload) => {
    for (var game of payload.games) {
      updateGame(game);
    }
    cleanUpGames(payload.games);
    console.log("current_games", payload)
  })

  channel.on('update_games', (payload) => {
    for (var game of payload.games) {
      updateGame(game);
    }
    cleanUpGames(payload.games);
    console.log("update_games", payload);
  })
}

export default socket
