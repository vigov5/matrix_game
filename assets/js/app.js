// Brunch automatically concatenates all files in your
// watched paths. Those paths can be configured at
// config.paths.watched in "brunch-config.js".
//
// However, those files will only be executed if
// explicitly imported. The only exception are files
// in vendor, which are never wrapped in imports and
// therefore are always executed.

// Import dependencies
//
// If you no longer want to use a dependency, remember
// to also remove its path from "config.paths.watched".
import "phoenix_html"

// Import local files
//
// Local files can be imported directly using relative
// paths "./socket" or full ones "web/static/js/socket".

import socket from "./socket"
import game from "./game"

if (window.location.pathname.indexOf("/game/") != -1) {
    let nick = localStorage.getItem('nick');
    if (!nick || nick.length > 10) {
        alert('Please set nickname first then re-join');
        window.location = "/";
    } else {
        game.start(socket);
    }
}

function matrix_alert(content) {
	$("#alert_content").html(content);
	$("#matrix_dialog").show();
}

$("#alert_close_btn").click(function(){
	$("#matrix_dialog").hide();
});

$(document).ready(function () {
    $('select').formSelect();
    $('#show_password').on("click", () => {
        var x = document.getElementById("password");
        if (x.type === "password") {
            x.type = "text";
        } else {
            x.type = "password";
        }
    });
});
