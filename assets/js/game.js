function matrix_alert (content) {
    $("#alert_content").html(content);
    $("#matrix_dialog").show();
}

$("#alert_close_btn").click(function () {
    $("#matrix_dialog").hide();
});

var game = {
    start: function (socket) {
        let nick = localStorage.getItem('nick');
        let needAuth = window.locked && !window.full;
        let password = "";
        if (needAuth) {
            password = prompt(`Game locked! Please enter password:`, "");
        }

        if (password == null) {
            window.location = "/";
        }

        var channel = socket.channel("game:" + window.GameId, { nick: nick, password: password });
        game.channel = channel;
        game.socket = socket;
        channel.join()
            .receive("ok", resp => {
                console.log("Joined game successfully", resp)
                gtag('event', 'Players', { 'event_category': 'joined', 'event_label': nick });
                game.init_game(resp);
                $("#ready").show();
                game.continueStart();
            })
            .receive("error", resp => {
                console.log("Unable to join", resp);
                gtag('event', 'Players', { 'event_category': 'denied', 'event_label': resp.reason });
                alert(resp.reason);
                window.location = "/";
            })
    },
    continueStart: function () {
        let channel = game.channel;
        $("#ready").on("click", resp => {
            channel.push("game:set_ready", { ready: true })
                .receive("ok", resp => {
                    $("#ready").remove();
                    console.log("Set ready successfully", resp);
                })
                .receive("error", resp => { console.log("Set ready failed", resp) })
        })

        channel.on('game:new_piece', (payload) => {
            console.log("game:new_piece", payload);
            game.turn += 1;
            game.genarate_new_step(payload.piece);
            game.start_counter(payload.piece);
            for (var i = 1; i <= game.number_of_players; i++)
                game.mark_player(i, "");
        })

        channel.on('game:player_joined', (payload) => {
            console.log("game:player_joined", payload);
            game.init_game(payload);
        })

        channel.on('game:stopped', (payload) => {
            console.log("game:stopped", payload);
            gtag('event', 'Games', { 'event_category': 'completed', 'event_label': 'completed' });
            matrix_alert("Game ended normally");
        })

        channel.on('game:started', (payload) => {
            console.log("game:started", payload);
            gtag('event', 'Games', { 'event_category': 'started', 'event_label': 'started' });
        })

        channel.on('game:player_left', (payload) => {
            console.log("game:player_left", payload);
            matrix_alert("Player " + payload.player_id + " left the game. The game will be ended.");
        })

        channel.on('game:over', (payload) => {
            console.log("game:over", payload);
            gtag('event', 'Games', { 'event_category': 'unfinished', 'event_label': 'unfinished' });
        })

        channel.on('game:place_piece', (payload) => {
            console.log("game:place_piece", payload);
            var p_no = payload.game.players.indexOf(payload.player_id) + 1;
            if (payload.player_id == window.PlayerId) {
                game.own_piece[p_no].setDraggable(false);
                game.own_piece[p_no].mouseEnabled = false;
                var tween = new Konva.Tween({
                    node: game.own_piece[p_no],
                    duration: 0.2,
                    easing: Konva.Easings.EaseIn,
                    scaleX: 1,
                    scaleY: 1,
                    x: 625 + game.BASE * 0.5,
                    y: 250 + game.BASE * 1.5,
                });
                tween.play();
            }
            game.set_number_in(p_no, payload.piece.x, payload.piece.y / 3, payload.piece.values);
            game.mark_player(p_no, "✔");
            if (game.isSpectator) {
                game.own_piece[p_no].setVisible(false);
                window["number_layer_" + p_no].draw();
            }
        })
    },
    init_game: function (resp) {
        $("#game").html("");
        game.number_of_players = resp.game.players.length;
        game.own_player_number = resp.game.players.indexOf(window.PlayerId) + 1;
        game.PLAYER_NAMES = resp.game.players;
        game.PLAYER_NICKS = resp.game.player_nicks;
        game.LAST_MOVES = [];
        game.PLAYER_COLORS = ["#2ecc71", "#3498db", "#9b59b6", "#e74c3c"];
        game.PLAYER_TINT_COLORS = ["#25A35A", "#2A7AAF", "#7C4792", "#B93D30"];
        game.isSpectator = game.PLAYER_NAMES.indexOf(window.PlayerId) == -1;
        game.timeLimit = resp.game.time_limit
        game.own_piece = [];
        game.turn = resp.game.turn;

        game.SCALE = !game.isSpectator ? 1 : game.number_of_players > 2 ? 0.6 : 0.8;
        game.STAGEWIDTH = 700;
        game.STAGEHEIGHT = 500;
        game.BASE = 50;

        game.NUMBER_COLOR = "#2c3e50";

        if (!game.isSpectator) {
            game.load_player(resp.game, game.own_player_number);
        } else {
            game.own_player_number = 1;
            window.PlayerId = game.PLAYER_NAMES[0];
            game.load_player(resp.game, game.own_player_number);
            $("#ready").remove();
        }

        if (game.timeLimit == 0) {
            $("#counter").remove();
        } else {
            $("#current_sec").text(`${game.timeLimit}`);
        }

        game.PLAYER_NAMES.forEach(name => {
            if (name != window.PlayerId)
                game.load_player(resp.game, resp.game.players.indexOf(name) + 1);
        });

        if (!game.isSpectator) {
            window["stage_" + game.own_player_number].on('dragstart', function (evt) {
                var target = evt.target;
                target.setAttrs({
                    scale: {
                        x: 1.2,
                        y: 1.2
                    }
                });
            });

            window["stage_" + game.own_player_number].on('dragend', function (evt) {
                var target = evt.target;
                var diff_x = target.x() + game.BASE * 0.25 - 150 - game.BASE * 0.5;
                var diff_y = target.y() + game.BASE * 0.5 - 25 - game.BASE * 1.5;
                var hit = false;
                var target_x = Math.round(diff_x / game.BASE);
                var target_y = Math.round(diff_y / 3 / game.BASE);
                if (Math.round(diff_x / game.BASE) <= 8 && Math.round(diff_x / game.BASE) >= 0 && Math.round(diff_y / 3 / game.BASE) <= 2 && Math.round(diff_y / 3 / game.BASE) >= 0 && diff_x % game.BASE < game.BASE * 0.5 && diff_y % (3 * game.BASE) < game.BASE) {
                    if (!window["group_" + game.own_player_number + "_" + target_x + "_" + target_y].visible()) {
                        hit = true;
                        target.setDraggable(false);
                    }
                }
                var tween = new Konva.Tween({
                    node: game.own_piece[game.own_player_number],
                    duration: hit ? 0.5 : 0.2,
                    easing: hit ? Konva.Easings.ElasticEaseOut : Konva.Easings.EaseIn,
                    scaleX: 1,
                    scaleY: 1,
                    x: hit ? 150 + target_x * game.BASE + game.BASE * 0.5 : 625 + game.BASE * 0.5,
                    y: hit ? 25 + target_y * game.BASE * 3 + game.BASE * 1.5 : 250 + game.BASE * 1.5,
                    onFinish: function () {
                        if (hit) {
                            var target = game.own_piece[game.own_player_number];
                            target.setVisible(false);
                            target.setX(800);
                            target.setY(250 + game.BASE * 1.5);
                            game.channel.push("game:place_piece", { x: target_x, y: target_y * 3, turn: game.turn })
                                .receive("ok", resp => {
                                    console.log("Placed successfully", resp);
                                })
                                .receive("error", resp => {
                                    console.log("Place failed", resp);
                                });
                        }
                    }
                });
                tween.play();
            });
        }

        game.state = new Array(game.number_of_players);
        for (var i = 0; i < game.number_of_players; i++) {
            let player_id = game.PLAYER_NAMES[i];
            let grid = resp.game.player_boards[player_id].grid;
            game.state[i] = new Array(13);
            for (var j = 0; j <= 12; j++) {
                game.state[i][j] = new Array(13);
                for (var k = 0; k <= 12; k++) {
                    if (j >= 2 && j <= 10 && k >= 2 & k <= 10) {
                        game.state[i][j][k] = grid[`${j - 2}${k - 2}`];

                        let x = k - 2;
                        let y = Math.floor((j - 2) / 3);
                        let t = (j - 2) % 3;

                        if (game.state[i][j][k] != -1) {
                            window["text_" + (i + 1) + "_" + x + "_" + y + "_" + t].setText(game.state[i][j][k] + "");
                            window["group_" + (i + 1) + "_" + x + "_" + y].setVisible(true);
                            window["group_" + (i + 1) + "_" + x + "_" + y].draw();
                        }
                    } else {
                        game.state[i][j][k] = -1;
                    }
                }
            }
        }
        game.calculate_score_and_draw_lines();
    },
    load_player: function (resp_game, p_no) {
        let scaledWidth = game.STAGEWIDTH * game.SCALE;
        let scaledHeight = game.STAGEHEIGHT * game.SCALE;
        $("#game").append(`<div id="stage_player_${p_no}" class="stage_player" style="width: ${scaledWidth}px; height: ${scaledHeight}px;"></div>`);
        window["stage_" + p_no] = new Konva.Stage({
            container: "stage_player_" + p_no,
        });

        window["stage_" + p_no].width(scaledWidth);
        window["stage_" + p_no].height(scaledHeight);
        window["stage_" + p_no].scale({ x: game.SCALE, y: game.SCALE });

        window["background_layer" + p_no] = new Konva.Layer();
        window["score_layer_" + p_no] = new Konva.Layer();
        window["number_layer_" + p_no] = new Konva.Layer();
        for (var i = 0; i < 10; i++) {
            window["background_layer" + p_no].add(new Konva.Line({
                points: [150 + i * game.BASE, 25, 150 + i * game.BASE, 475],
                stroke: game.NUMBER_COLOR,
                strokeWidth: i % 3 == 0 ? 3 : 2
            }));
            window["background_layer" + p_no].add(new Konva.Line({
                points: [150, 25 + i * game.BASE, 600, 25 + i * game.BASE],
                stroke: game.NUMBER_COLOR,
                strokeWidth: i % 3 == 0 ? 3 : 2
            }));
            if (i == 9) break;
            for (var j = 0; j < 3; j++) {
                window["group_" + p_no + "_" + i + "_" + j] = new Konva.Group({
                    x: 150 + i * game.BASE + game.BASE * 0.5,
                    y: 25 + j * game.BASE * 3 + game.BASE * 1.5,
                    visible: false
                });
                window["group_" + p_no + "_" + i + "_" + j].add(new Konva.Rect({
                    x: - game.BASE * 0.5,
                    y: - game.BASE * 1.5,
                    width: game.BASE,
                    height: game.BASE * 3,
                    fill: game.PLAYER_COLORS[p_no - 1]
                }));
                for (var k = 0; k < 3; k++) {
                    window["text_" + p_no + "_" + i + "_" + j + "_" + k] = new Konva.Text({
                        x: - game.BASE * 0.5,
                        y: 10 + k * game.BASE - game.BASE * 1.5,
                        width: game.BASE,
                        height: game.BASE,
                        text: "8",
                        fontSize: 33,
                        fontFamily: "Menlo, Calibri",
                        fill: game.NUMBER_COLOR,
                        fontStyle: "bold",
                        align: "center",
                        margin: "auto"
                    });
                    window["group_" + p_no + "_" + i + "_" + j].add(window["text_" + p_no + "_" + i + "_" + j + "_" + k]);
                }
                window["number_layer_" + p_no].add(window["group_" + p_no + "_" + i + "_" + j]);
            }
            for (var j = 0; j < 9; j++) {
                window["line_" + p_no + "_" + j + "_" + i + "_" + "lr"] = new Konva.Line({
                    points: [150 + game.BASE * i, 25 + game.BASE * j + game.BASE / 2, 150 + game.BASE * (i + 1), 25 + game.BASE * j + game.BASE / 2],
                    stroke: game.NUMBER_COLOR,
                    strokeWidth: 20,
                    opacity: 0
                });
                window["number_layer_" + p_no].add(window["line_" + p_no + "_" + j + "_" + i + "_" + "lr"]);
                window["line_" + p_no + "_" + j + "_" + i + "_" + "tb"] = new Konva.Line({
                    points: [150 + game.BASE * i + game.BASE / 2, 25 + game.BASE * j, 150 + game.BASE * i + game.BASE / 2, 25 + game.BASE * (j + 1)],
                    stroke: game.NUMBER_COLOR,
                    strokeWidth: 20,
                    opacity: 0
                });
                window["number_layer_" + p_no].add(window["line_" + p_no + "_" + j + "_" + i + "_" + "tb"]);
                window["line_" + p_no + "_" + j + "_" + i + "_" + "tlbr"] = new Konva.Line({
                    points: [150 + game.BASE * i, 25 + game.BASE * j, 150 + game.BASE * (i + 1), 25 + game.BASE * (j + 1)],
                    stroke: game.NUMBER_COLOR,
                    strokeWidth: 20,
                    opacity: 0
                });
                window["number_layer_" + p_no].add(window["line_" + p_no + "_" + j + "_" + i + "_" + "tlbr"]);
                window["line_" + p_no + "_" + j + "_" + i + "_" + "trbl"] = new Konva.Line({
                    points: [150 + game.BASE * (i + 1), 25 + game.BASE * j, 150 + game.BASE * i, 25 + game.BASE * (j + 1)],
                    stroke: game.NUMBER_COLOR,
                    strokeWidth: 20,
                    opacity: 0
                });
                window["number_layer_" + p_no].add(window["line_" + p_no + "_" + j + "_" + i + "_" + "trbl"]);
            }
        }
        for (var p_no_2 = 1; p_no_2 <= game.PLAYER_NICKS.length; p_no_2++) {
            window["player_nick_" + p_no + "_" + p_no_2] = new Konva.Text({
                x: 0,
                y: 35 + (p_no_2 - 1) * 1.5 * game.BASE,
                width: 150,
                text: game.PLAYER_NICKS[p_no_2 - 1],
                fontSize: 20,
                fontFamily: "Menlo, Calibri",
                fill: game.PLAYER_COLORS[p_no_2 - 1],
                fontStyle: p_no == p_no_2 ? "bold" : "normal",
                align: "center",
                margin: "auto"
            });
            window["score_layer_" + p_no].add(window["player_nick_" + p_no + "_" + p_no_2]);
            window["player_score_" + p_no + "_" + p_no_2] = new Konva.Text({
                x: 0,
                y: 35 + (p_no_2 - 1) * 1.5 * game.BASE + 2 * game.BASE / 3,
                width: 150,
                text: "0",
                fontSize: 20,
                fontFamily: "Menlo, Calibri",
                fill: game.PLAYER_COLORS[p_no_2 - 1],
                fontStyle: p_no == p_no_2 ? "bold" : "normal",
                align: "center",
                margin: "auto"
            });
            window["score_layer_" + p_no].add(window["player_score_" + p_no + "_" + p_no_2]);
        }
        window["stage_" + p_no].add(window["background_layer" + p_no]);
        window["stage_" + p_no].add(window["score_layer_" + p_no]);
        window["stage_" + p_no].add(window["number_layer_" + p_no]);

        // Current piece
        var current_piece = [0, 0, 0];
        if (resp_game.piece_values.length) {
            var current_piece = resp_game.piece_values;
        }
        game.own_piece[p_no] = new Konva.Group({
            x: 800,
            y: 250 + game.BASE * 1.5,
            visible: resp_game.piece_values.length && game.isSpectator
        });
        window["own_piece_" + p_no] = game.own_piece[p_no];
        game.own_piece[p_no].add(new Konva.Rect({
            x: - game.BASE * 0.5,
            y: - game.BASE * 1.5,
            width: game.BASE,
            height: game.BASE * 3,
            fill: game.PLAYER_COLORS[p_no - 1]
        }));

        if (!game.isSpectator) {
            if ((typeof window.orientation !== "undefined") || (navigator.userAgent.indexOf('IEMobile') !== -1)) {
                game.own_piece[p_no].add(new Konva.Circle({
                    x: - game.BASE * 0.5 + game.BASE / 2,
                    y: - game.BASE * 1.5 + game.BASE * 3 + game.BASE,
                    radius: game.BASE / 2,
                    opacity: 0.2,
                    fill: game.PLAYER_COLORS[p_no - 1]
                }));
            }
        }

        for (var k = 0; k < 3; k++) {
            window[`piece_text_${p_no}_${k}`] = new Konva.Text({
                x: - game.BASE * 0.5,
                y: 10 + k * game.BASE - game.BASE * 1.5,
                width: game.BASE,
                height: game.BASE,
                text: current_piece[k],
                fontSize: 33,
                fontFamily: "Menlo, Calibri",
                fill: game.NUMBER_COLOR,
                fontStyle: "bold",
                align: "center",
                margin: "auto"
            });
            game.own_piece[p_no].add(window[`piece_text_${p_no}_${k}`]);
        }

        window["number_layer_" + p_no].add(game.own_piece[p_no]);
        window["number_layer_" + p_no].draw();
    },
    genarate_new_step: function (piece) {
        var start = game.own_player_number;
        var end = start;
        if (game.isSpectator) {
            var start = 1;
            var end = game.number_of_players;
        }
        for (var p_no = start; p_no <= end; p_no++) {
            game.own_piece[p_no].setVisible(true);
            game.own_piece[p_no].mouseEnabled = true;
            for (var k = 0; k < 3; k++) {
                window[`piece_text_${p_no}_${k}`].setText(piece[k] + "");
            }
            var tween = new Konva.Tween({
                node: game.own_piece[p_no],
                duration: 0.2,
                easing: Konva.Easings.EaseIn,
                x: 625 + game.BASE * 0.5,
                onFinish: function () {
                    if (!game.isSpectator) {
                        game.own_piece[this.p_no].setDraggable(true);
                    }
                }
            });
            tween.p_no = p_no,
                tween.play();
        }
    },
    set_number_in: function (p_no, x, y, values) {
        for (var k = 0; k < 3; k++) {
            window["text_" + p_no + "_" + x + "_" + y + "_" + k].setText(values[k] + "");
            game.state[p_no - 1][y * 3 + k + 2][x + 2] = values[k];
        }
        window["group_" + p_no + "_" + x + "_" + y].setVisible(true);
        if (game.isSpectator) {
            if (game.LAST_MOVES[p_no]) {
                let last_move = game.LAST_MOVES[p_no];
                var rect = window["group_" + p_no + "_" + last_move.x + "_" + last_move.y].find('Rect')[0];
                rect.fill(game.PLAYER_COLORS[p_no - 1]);
                window["group_" + p_no + "_" + last_move.x + "_" + last_move.y].draw();
            }
            var rect = window["group_" + p_no + "_" + x + "_" + y].find('Rect')[0];
            rect.fill(game.PLAYER_TINT_COLORS[p_no - 1]);
        }
        window["group_" + p_no + "_" + x + "_" + y].draw();
        game.LAST_MOVES[p_no] = { x, y };
        game.own_piece[p_no].setVisible(false);

        var player_scores = game.calculate_score_and_draw_lines();
        game.reorder_scores(player_scores);
    },
    calculate_score_and_draw_lines: function () {
        var player_scores = [0, 0, 0, 0].splice(0, game.number_of_players);
        for (var i = 0; i < game.number_of_players; i++) {
            for (var j = 2; j <= 10; j++) {
                for (var k = 2; k <= 10; k++) {
                    // lr
                    if (game.same_values(game.state[i][j][k - 2], game.state[i][j][k - 1], game.state[i][j][k]) || game.same_values(game.state[i][j][k + 1], game.state[i][j][k - 1], game.state[i][j][k]) || game.same_values(game.state[i][j][k + 2], game.state[i][j][k + 1], game.state[i][j][k])) {
                        player_scores[i] += game.state[i][j][k];
                        var target = window["line_" + (i + 1) + "_" + (j - 2) + "_" + (k - 2) + "_" + "lr"];
                        if (target.opacity() == 0) {
                            var tween = new Konva.Tween({
                                node: target,
                                duration: 0.5,
                                easing: Konva.Easings.ElasticEaseOut,
                                opacity: 1,
                                strokeWidth: 2
                            });
                            tween.play();
                        }
                    }
                    // tb
                    if (game.same_values(game.state[i][j - 1][k], game.state[i][j - 2][k], game.state[i][j][k]) || game.same_values(game.state[i][j - 1][k], game.state[i][j + 1][k], game.state[i][j][k]) || game.same_values(game.state[i][j + 2][k], game.state[i][j + 1][k], game.state[i][j][k])) {
                        player_scores[i] += game.state[i][j][k] == 0 ? 10 : game.state[i][j][k];
                        var target = window["line_" + (i + 1) + "_" + (j - 2) + "_" + (k - 2) + "_" + "tb"];
                        if (target.opacity() == 0) {
                            var tween = new Konva.Tween({
                                node: target,
                                duration: 0.5,
                                easing: Konva.Easings.ElasticEaseOut,
                                opacity: 1,
                                strokeWidth: 2
                            });
                            tween.play();
                        }
                    }
                    // tlbr
                    if (game.same_values(game.state[i][j - 1][k - 1], game.state[i][j - 2][k - 2], game.state[i][j][k]) || game.same_values(game.state[i][j - 1][k - 1], game.state[i][j + 1][k + 1], game.state[i][j][k]) || game.same_values(game.state[i][j + 2][k + 2], game.state[i][j + 1][k + 1], game.state[i][j][k])) {
                        player_scores[i] += game.state[i][j][k] == 0 ? 10 : game.state[i][j][k];
                        var target = window["line_" + (i + 1) + "_" + (j - 2) + "_" + (k - 2) + "_" + "tlbr"];
                        if (target.opacity() == 0) {
                            var tween = new Konva.Tween({
                                node: target,
                                duration: 0.5,
                                easing: Konva.Easings.ElasticEaseOut,
                                opacity: 1,
                                strokeWidth: 2
                            });
                            tween.play();
                        }
                    }
                    // trbl
                    if (game.same_values(game.state[i][j - 1][k + 1], game.state[i][j - 2][k + 2], game.state[i][j][k]) || game.same_values(game.state[i][j - 1][k + 1], game.state[i][j + 1][k - 1], game.state[i][j][k]) || game.same_values(game.state[i][j + 2][k - 2], game.state[i][j + 1][k - 1], game.state[i][j][k])) {
                        player_scores[i] += game.state[i][j][k] == 0 ? 10 : game.state[i][j][k];
                        var target = window["line_" + (i + 1) + "_" + (j - 2) + "_" + (k - 2) + "_" + "trbl"];
                        if (target.opacity() == 0) {
                            var tween = new Konva.Tween({
                                node: target,
                                duration: 0.5,
                                easing: Konva.Easings.ElasticEaseOut,
                                opacity: 1,
                                strokeWidth: 2
                            });
                            tween.play();
                        }
                    }
                }
            }
        }
        game.set_scores(player_scores);

        return player_scores;
    },
    reorder_scores: function (player_scores) {
        for (var p_no = 1; p_no <= game.PLAYER_NAMES.length; p_no++) {
            var points = player_scores.slice(0);
            points.sort(function (a, b) { return b - a });
            for (var p_no_2 = 1; p_no_2 <= game.PLAYER_NAMES.length; p_no_2++) {
                var nick = window["player_nick_" + p_no + "_" + p_no_2];
                var current = Math.floor((nick.y() - 35) / 1.5 / game.BASE) + 1;
                var next = points.indexOf(player_scores[p_no_2 - 1]);
                if (next + 1 != current) {
                    // TODO better optimization
                    var tween = new Konva.Tween({
                        node: nick,
                        duration: 0.5,
                        easing: Konva.Easings.ElasticEaseOut,
                        y: 35 + next * 1.5 * game.BASE
                    });
                    tween.play();

                    var tween = new Konva.Tween({
                        node: window["player_score_" + p_no + "_" + p_no_2],
                        duration: 0.5,
                        easing: Konva.Easings.ElasticEaseOut,
                        y: 35 + next * 1.5 * game.BASE + 2 * game.BASE / 3
                    });
                    tween.play();
                }
                points[next] = -1;
            }
        }
    },
    same_values: function (a, b, c) {
        return (a == b && b == c && a != -1);
    },
    set_scores: function (player_scores) {
        for (var p_no = 1; p_no <= game.number_of_players; p_no++) {
            for (var p_no_2 = 1; p_no_2 <= game.number_of_players; p_no_2++) {
                window["player_score_" + p_no + "_" + p_no_2].setText(player_scores[p_no_2 - 1] + "");
            }
            window["score_layer_" + p_no].draw();
        }
    },
    mark_player: function (p_no, text) {
        for (var idx = 1; idx <= game.number_of_players; idx++) {
            var nameText = window[`player_nick_${idx}_${p_no}`];
            nameText.setText(text + game.PLAYER_NICKS[p_no - 1]);
            window["score_layer_" + idx].draw();
        }
    },
    start_counter: function () {
        clearInterval(game.tick);
        $("#current_sec").text(`${game.timeLimit}`);
        game.tick = setInterval(function () {
            let sec = parseInt($("#current_sec").text())
            if (sec > 0) {
                $("#current_sec").text(`${sec - 1}`);
            } else {
                clearInterval(game.tick);
            }
        }, 1000);
    }
}
export default game
