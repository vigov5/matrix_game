########################################
# 1. Build nodejs frontend
########################################
FROM node:12.11.1-alpine as build-node

# prepare build dir
RUN mkdir -p /app/assets
WORKDIR /app

# set build ENV
ENV NODE_ENV=prod

# install npm dependencies
COPY assets/package.json assets/package-lock.json ./assets/
COPY deps/phoenix deps/phoenix
COPY deps/phoenix_html deps/phoenix_html
RUN cd assets && npm install

# build assets
COPY assets ./assets/
RUN cd assets && npm run deploy


########################################
# 2. Build elixir backend
########################################
FROM elixir:1.9.1-alpine as build-elixir

# install build dependencies
RUN apk add --update git

# prepare build dir
RUN mkdir /app
WORKDIR /app

# install hex + rebar
RUN mix local.hex --force && \
    mix local.rebar --force

# set build ENV
ENV MIX_ENV=prod

# install dependencies
COPY mix.exs mix.lock ./
RUN mix deps.get

# compile dependencies
COPY config ./config/
RUN mix deps.compile

# copy only elixir files to keep the cache
COPY lib ./lib/
COPY priv ./priv/
# COPY rel ./rel/

# copy assets from node build
COPY --from=build-node /app/priv/static ./priv/static
RUN mix phx.digest

# build release
RUN mix release


########################################
# 3. Build release image
########################################
FROM alpine:3.9
RUN apk add --update bash openssl
WORKDIR /app

COPY --from=build-elixir /app/_build/prod/rel/matrix_game ./
RUN chown -R nobody: /app

USER nobody
ARG VERSION
ENV VERSION=$VERSION
ENV REPLACE_OS_VARS=true
EXPOSE 4000

ENTRYPOINT ["/app/bin/matrix_game"]
CMD ["start"]